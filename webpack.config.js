const path = require('path');
const webpack = require('webpack');
const forkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  mode: process.env.NODE_ENV || 'development',
  devtool: process.env.NODE_ENV === 'production' ? 'source-map' : 'cheap-source-map',
  resolve: {
    extensions: [ '.tsx', '.ts', '.js', '.scss', '.sass' ],
  },
  entry: {
    index: './src/index.tsx',
    convert2to6: './src/convert2to6/index.tsx',
  },
  output: {
    filename: '[chunkhash].js',
    path: path.resolve(__dirname, 'public'),
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: 'ts-loader?transpileOnly',
      },
      {
        test: /\.s[ac]ss$/,
        use: [ 'style-loader', 'css-loader', 'sass-loader' ],
      },
      {
        test: /\.(png|svg|jpg|gif|font)$/,
        use: [ 'file-loader' ],
      },
    ],
  },
  plugins: [
    new forkTsCheckerWebpackPlugin({ useTypescriptIncrementalApi: true }),
    new webpack.DefinePlugin({ IMGUR_CLIENT_ID: `'${process.env.IMGUR_CLIENT_ID}'` }),
    new HtmlWebpackPlugin({ template: 'src/index.html', chunks: ['index'], filename: 'index.html' }),
    new HtmlWebpackPlugin({ template: 'src/index.html', chunks: ['convert2to6'], filename: 'convert2to6/index.html' }),
    new FaviconsWebpackPlugin({ logo: path.resolve(__dirname, 'src/img/logo.png') }),
  ],
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    host: '0.0.0.0',
    port: 8080,
  },
};
