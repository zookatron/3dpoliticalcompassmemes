import memoize from 'memoizee';
import React, {useRef, useCallback} from 'react';
import ReactDOM from 'react-dom';
import {Button, File as FileComponent} from 'rbx';
import {ErrorBoundary} from '../ErrorBoundary';
import './index.scss';

const App = () => {
  const front = useRef<HTMLCanvasElement>(null);
  const back = useRef<HTMLCanvasElement>(null);
  const left = useRef<HTMLCanvasElement>(null);
  const right = useRef<HTMLCanvasElement>(null);
  const top = useRef<HTMLCanvasElement>(null);
  const bottom = useRef<HTMLCanvasElement>(null);

  // useEffect(() => {
  //   const onResize = () => {
  //     if(front.current) { front.current.width = window; }
  //   };
  //   window.addEventListener('resize', onResize, false);
  //   return () => window.removeEventListener('resize', onResize, false);
  // }, []);

  const convert2to6 = useCallback(() => {
    if (!front.current || !back.current || !left.current || !right.current || !top.current || !bottom.current) {
      return;
    }
    const frontContext = front.current.getContext('2d');
    const backContext = back.current.getContext('2d');
    const leftContext = left.current.getContext('2d');
    const rightContext = right.current.getContext('2d');
    const topContext = top.current.getContext('2d');
    const bottomContext = bottom.current.getContext('2d');
    if (!frontContext || !backContext || !leftContext || !rightContext || !topContext || !bottomContext) {
      return;
    }
    const frontHalfW = front.current.width / 2;
    const frontHalfH = front.current.height / 2;
    const backHalfW = back.current.width / 2;
    const backHalfH = back.current.height / 2;

    left.current.width = left.current.height = Math.max(front.current.width, front.current.height, back.current.width, back.current.height);
    const leftHalfW = left.current.width / 2;
    const leftHalfH = left.current.height / 2;
    leftContext.drawImage(back.current, backHalfW, 0, backHalfW, backHalfH, 0, 0, leftHalfW, leftHalfH);
    leftContext.drawImage(back.current, backHalfW, backHalfH, backHalfW, backHalfH, 0, leftHalfH, leftHalfW, leftHalfH);
    leftContext.drawImage(front.current, 0, 0, frontHalfW, frontHalfH, leftHalfW, 0, leftHalfW, leftHalfH);
    leftContext.drawImage(front.current, 0, frontHalfH, frontHalfW, frontHalfH, leftHalfW, leftHalfH, leftHalfW, leftHalfH);

    right.current.width = right.current.height = Math.max(front.current.width, front.current.height, back.current.width, back.current.height);
    const rightHalfW = right.current.width / 2;
    const rightHalfH = right.current.height / 2;
    rightContext.drawImage(back.current, 0, 0, backHalfW, backHalfH, rightHalfW, 0, rightHalfW, rightHalfH);
    rightContext.drawImage(back.current, 0, backHalfH, backHalfW, backHalfH, rightHalfW, rightHalfH, rightHalfW, rightHalfH);
    rightContext.drawImage(front.current, frontHalfW, 0, frontHalfW, frontHalfH, 0, 0, rightHalfW, rightHalfH);
    rightContext.drawImage(front.current, frontHalfW, frontHalfH, frontHalfW, frontHalfH, 0, rightHalfH, rightHalfW, rightHalfH);

    top.current.width = top.current.height = Math.max(front.current.width, front.current.height, back.current.width, back.current.height);
    const topHalfW = top.current.width / 2;
    const topHalfH = top.current.height / 2;
    topContext.drawImage(back.current, 0, 0, backHalfW, backHalfH, topHalfW, 0, topHalfW, topHalfH);
    topContext.drawImage(back.current, backHalfW, 0, backHalfW, backHalfH, 0, 0, topHalfW, topHalfH);
    topContext.drawImage(front.current, 0, 0, frontHalfW, frontHalfH, 0, topHalfH, topHalfW, topHalfH);
    topContext.drawImage(front.current, frontHalfW, 0, frontHalfW, frontHalfH, topHalfW, topHalfH, topHalfW, topHalfH);

    bottom.current.width = bottom.current.height = Math.max(front.current.width, front.current.height, back.current.width, back.current.height);
    const bottomHalfW = bottom.current.height / 2;
    const bottomHalfH = bottom.current.height / 2;
    bottomContext.drawImage(back.current, backHalfW, backHalfH, backHalfW, backHalfH, 0, bottomHalfH, bottomHalfW, bottomHalfH);
    bottomContext.drawImage(back.current, 0, backHalfH, backHalfW, backHalfH, bottomHalfW, bottomHalfH, bottomHalfW, bottomHalfH);
    bottomContext.drawImage(front.current, 0, frontHalfH, frontHalfW, frontHalfH, 0, 0, bottomHalfW, bottomHalfH);
    bottomContext.drawImage(front.current, frontHalfW, frontHalfH, frontHalfW, frontHalfH, bottomHalfW, 0, bottomHalfW, bottomHalfH);
  }, [front, back, left, right, top, bottom]);

  const upload = useCallback(memoize((side: React.RefObject<HTMLCanvasElement>) => (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!side.current || !event.target || !event.target.files) {
      return;
    }
    const context = side.current.getContext('2d');
    if (!context) {
      return;
    }
    const image = new Image();
    image.addEventListener('load', () => {
        side.current!.width = image.width;
        side.current!.height = image.height;
        context.drawImage(image, 0, 0);
        URL.revokeObjectURL(image.src);
        convert2to6();
    });
    image.src = URL.createObjectURL(event.target.files[0]);
  }), [convert2to6]);

  const download = useCallback(memoize((name: string, side: React.RefObject<HTMLCanvasElement>) => () => {
    if (!side.current) {
      return;
    }
    const link = document.createElement('a');
    link.download = `${name}.png`;
    link.href = side.current.toDataURL('image/png');
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }), []);

  return <>
    <div className="image"><canvas ref={front} />
      <FileComponent>
        <FileComponent.Label>
          <FileComponent.Input onChange={upload(front)}/>
          <FileComponent.CTA>
            <FileComponent.Label as="span">Upload Front</FileComponent.Label>
          </FileComponent.CTA>
        </FileComponent.Label>
      </FileComponent>
    </div>
    <div className="image"><canvas ref={left} /><Button onClick={download('left', left)}>Download Left</Button></div>
    <div className="image"><canvas ref={top} /><Button onClick={download('top', top)}>Download Top</Button></div>
    <div className="image"><canvas ref={back} />
      <FileComponent>
        <FileComponent.Label>
          <FileComponent.Input onChange={upload(back)}/>
          <FileComponent.CTA>
            <FileComponent.Label as="span">Upload Back</FileComponent.Label>
          </FileComponent.CTA>
        </FileComponent.Label>
      </FileComponent>
    </div>
    <div className="image"><canvas ref={right} /><Button onClick={download('right', right)}>Download Right</Button></div>
    <div className="image"><canvas ref={bottom} /><Button onClick={download('bottom', bottom)}>Download Bottom</Button></div>
  </>;
};

ReactDOM.render(<ErrorBoundary><App /></ErrorBoundary>, document.getElementById('container'));
