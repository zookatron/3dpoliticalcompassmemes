import {isArray, isPlainObject, flatten, values} from 'lodash-es';
import React from 'react';
import {configure} from 'mobx';
import {observer} from 'mobx-react-lite';
import axios, {AxiosResponse} from 'axios';
import {Color} from 'three';

export function makeWithState<StateType>(state: StateType):
  <PropType extends {}>(WrappedComponent: React.FunctionComponent<PropType & { state: StateType }>) => React.FunctionComponent<PropType> {
  configure({computedRequiresReaction: true, enforceActions: 'observed'});
  // eslint-disable-next-line arrow-body-style
  return <PropType extends {}>(WrappedComponent: React.FunctionComponent<PropType & { state: StateType }>): React.FunctionComponent<PropType> => {
    const ObserverComponent = observer(WrappedComponent);
    return (props: PropType) => <ObserverComponent {...props} state={state} />;
  };
}

export function degToRad(degrees: number) {
  return degrees * Math.PI / 180;
}

export function colorWithLightness(color: Color, lightness: number) {
  const newColor = new Color();
  const hsl = {h: 0, s: 0, l: 0};
  color.getHSL(hsl);
  hsl.l = lightness;
  newColor.setHSL(hsl.h, hsl.s, hsl.l);
  return newColor;
}

export function getLeaves(structure: unknown): unknown[] {
  if (isArray(structure)) {
    return flatten(structure.map(getLeaves));
  } else if (isPlainObject(structure)) {
    return flatten(values(structure).map(getLeaves));
  } else {
    return [structure];
  }
}

const toBase64 = (file: File) => new Promise<string>((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => {
    const dataUrl = reader.result as string;
    resolve(dataUrl.split('base64,')[1]);
  };
  reader.onerror = error => reject(error);
});

export async function uploadToImgur(file: File): Promise<string> {
  const data = new FormData();
  data.set('image', await toBase64(file));
  data.set('type', 'base64');
  const response: AxiosResponse<{ data: { link: string } }> = await axios({
    method: 'post',
    url: 'https://api.imgur.com/3/image',
    data,
    headers: {
      Authorization: `Client-ID ${IMGUR_CLIENT_ID}`,
      Accept: 'application/json',
    },
  });
  return response.data.data.link;
}
