import {isArray, isString, isNumber} from 'lodash-es';
import {observable, runInAction, toJS} from 'mobx';
import msgpack from 'msgpack-lite';
import {fromByteArray, toByteArray} from 'base64-js';
import {makeWithState} from './utils';
import {Color} from 'three';

// ----------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------

export interface StateType {
  nonexistant: boolean | null,
  editing: boolean,
  interacted: boolean,
  axes: {
    [axis in 'x' | 'y' | 'z']: { poles: { [pole in 'negative' | 'positive']: { label: string, texture: { url: string, rotation: number } } }, color: Color };
  };
}

const state = observable({
  nonexistant: null,
  editing: false,
  interacted: false,
  axes: {
    x: {poles: {
      negative: {label: '', texture: {url: '', rotation: 0}},
      positive: {label: '', texture: {url: '', rotation: 0}},
    }, color: new Color(1, 0, 0)},
    y: {poles: {
      negative: {label: '', texture: {url: '', rotation: 0}},
      positive: {label: '', texture: {url: '', rotation: 0}},
    }, color: new Color(0, 0, 1)},
    z: {poles: {
      negative: {label: '', texture: {url: '', rotation: 0}},
      positive: {label: '', texture: {url: '', rotation: 0}},
    }, color: new Color(0, 1, 0)},
  },
} as StateType);

export const withState = makeWithState<StateType>(state);

export const updateTexture = (axis: keyof StateType['axes'], pole: keyof StateType['axes']['x']['poles'], texture: { url: string, rotation: number }) => {
  runInAction(() => state.axes[axis].poles[pole].texture = texture);
};

export const updateAxis = (name: keyof StateType['axes'], axis: StateType['axes']['x']) => {
  runInAction(() => state.axes[name] = axis);
};

export const updateEditing = (editing: boolean) => {
  runInAction(() => state.editing = editing);
};

export const updateInteracted = (interacted: boolean) => {
  runInAction(() => state.interacted = interacted);
};

export const save = () => {
  const source = toJS(state);
  const data: unknown[] = [1];
  for (const axis of ['x', 'y', 'z'] as ('x' | 'y' | 'z')[]) {
    for (const pole of ['negative', 'positive'] as ('negative' | 'positive')[]) {
      data.push(source.axes[axis].poles[pole].label);
      data.push(source.axes[axis].poles[pole].texture.url);
      data.push(source.axes[axis].poles[pole].texture.rotation);
    }
    data.push(source.axes[axis].color.getHexString());
  }
  return fromByteArray(msgpack.encode(data));
};

export const load = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const encodedData = urlParams.get('data');
  if (!encodedData) {
    runInAction(() => state.nonexistant = true);
    return;
  }
  const data = msgpack.decode(toByteArray(encodedData));
  if (!isArray(data)) {
    throw new Error('Invalid encoded data: No root array found!');
  }
  let dataElement = 0;
  if (data[dataElement++] !== 1) {
    throw new Error('Invalid encoded data: No version number found!');
  }
  runInAction(() => {
    for (const axis of ['x', 'y', 'z'] as ('x' | 'y' | 'z')[]) {
      for (const pole of ['negative', 'positive'] as ('negative' | 'positive')[]) {
        const label = data[dataElement++];
        if (!isString(label)) {
          throw new Error('Invalid encoded data: Label not encoded as string!');
        }
        state.axes[axis].poles[pole].label = label;
        const url = data[dataElement++];
        if (!isString(url)) {
          throw new Error('Invalid encoded data: URL not encoded as string!');
        }
        state.axes[axis].poles[pole].texture.url = url;
        const rotation = data[dataElement++];
        if (!isNumber(rotation)) {
          throw new Error('Invalid encoded data: rotation not encoded as number!');
        }
        state.axes[axis].poles[pole].texture.rotation = rotation;
      }
      const color = data[dataElement++];
      if (!isString(color)) {
        throw new Error('Invalid encoded data: Color not encoded as string!');
      }
      state.axes[axis].color = new Color(`#${color}`);
    }
    state.nonexistant = false;
  });
};
