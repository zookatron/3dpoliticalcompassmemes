import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import {ErrorBoundary} from './ErrorBoundary';
import {Scene} from './Scene';
import {withState, StateType, load} from './state';
import {EditorOverlay} from './EditorOverlay';
import {ViewerOverlay} from './ViewerOverlay';
import './index.scss';

const App = withState(({state}: { state: StateType }) => {
  useEffect(() => load(), []);
  return state.nonexistant === null ? <></> : state.nonexistant ? <div className="not-found"><h1>404: Not found</h1></div> : <>
    <Scene/>
    { state.editing ? <EditorOverlay/> : <ViewerOverlay/> }
  </>;
});

ReactDOM.render(<ErrorBoundary><App /></ErrorBoundary>, document.getElementById('container'));
