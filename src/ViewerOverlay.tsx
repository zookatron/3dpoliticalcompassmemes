import React, {useCallback} from 'react';
import classnames from 'classnames';
import {Icon} from 'rbx';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit} from '@fortawesome/free-solid-svg-icons';
import {withState, StateType, updateEditing} from './state';

export const ViewerOverlay = withState(({state}: { state: StateType }) => {
  const edit = useCallback(() => updateEditing(true), []);

  return <div className="overlay">
    <div className="edit" onClick={edit}><Icon size="large"><FontAwesomeIcon icon={faEdit} /></Icon></div>
    <div className={ classnames('instructions', state.interacted && 'hidden') }>Click and drag to rotate, scroll to zoom</div>
  </div>;
});
