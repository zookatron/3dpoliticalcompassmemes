import {merge} from 'lodash-es';
import memoize from 'memoizee';
import React, {useState, useCallback} from 'react';
import {withState, StateType, updateTexture, updateAxis, save} from './state';
import {Button, Modal, Notification, Delete, Level, Field, Control, Label, Input, Icon, File as FileComponent} from 'rbx';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSave, faUpload} from '@fortawesome/free-solid-svg-icons';
import {SliderPicker, ColorResult} from 'react-color';
import {toJS} from 'mobx';
import {Color} from 'three';
import {colorWithLightness, uploadToImgur} from './utils';

const axisPoleLabels = {
  x: {negative: 'Left', positive: 'Right'},
  y: {negative: 'Bottom', positive: 'Top'},
  z: {negative: 'Back', positive: 'Front'},
};

export const EditorOverlay = withState(({state}: { state: StateType }) => {
  const [editingTexture, setEditingTexture] = useState<{
    axis: keyof StateType['axes'],
    pole: keyof StateType['axes']['x']['poles'],
    texture: { url: string, rotation: number },
    loading: boolean,
  } | null>(null);
  const [editingAxis, setEditingAxis] = useState<{
    name: keyof StateType['axes'],
    axis: StateType['axes']['x'],
  } | null>(null);
  const [showingUrl, setShowingUrl] = useState<string | null>(null);

  const showUrl = useCallback(() => setShowingUrl(`https://zookatron.gitlab.io/3dpoliticalcompassmemes/?data=${encodeURIComponent(save())}`), []);
  const cancelShowingUrl = useCallback(() => setShowingUrl(null), [setShowingUrl]);
  const selectUrl = useCallback((event: MouseEvent) => {
    (event.target as HTMLInputElement)?.select();
  }, []);
  const editTexture = useCallback(memoize((axis: keyof StateType['axes'], pole: keyof StateType['axes']['x']['poles']) => () => {
    setEditingTexture({axis, pole, texture: toJS(state.axes[axis].poles[pole].texture), loading: false});
  }), [state, setEditingTexture]);
  const cancelEditTexture = useCallback(() => editingTexture?.loading ? null : setEditingTexture(null), [editingTexture, setEditingTexture]);
  const saveTextureUrl = useCallback(() => {
    if (!editingTexture) {
      return;
    }
    updateTexture(editingTexture.axis, editingTexture.pole, editingTexture.texture);
    setEditingTexture(null);
  }, [editingTexture, setEditingTexture]);
  const changeTextureUrl = useCallback((event: { target: { value: string } }) => {
    setEditingTexture(merge({}, editingTexture, {texture: {url: event.target.value}}));
  }, [editingTexture, setEditingTexture]);
  const changeTextureRotation = useCallback(memoize((rotation: number) => () => {
    setEditingTexture(merge({}, editingTexture, {texture: {rotation}}));
  }), [editingTexture, setEditingTexture]);
  const editAxis = useCallback(memoize((axis: keyof StateType['axes']) => () => {
    setEditingAxis({name: axis, axis: toJS(state).axes[axis]});
  }), [state, setEditingAxis]);
  const changeAxisNegativeLabel = useCallback((event: { target: { value: string } }) => {
    setEditingAxis(merge({}, editingAxis, {axis: {poles: {negative: {label: event.target.value}}}}));
  }, [editingAxis, setEditingAxis]);
  const changeAxisPositiveLabel = useCallback((event: { target: { value: string } }) => {
    setEditingAxis(merge({}, editingAxis, {axis: {poles: {positive: {label: event.target.value}}}}));
  }, [editingAxis, setEditingAxis]);
  const changeAxisColor = useCallback((color: ColorResult) => {
    setEditingAxis(merge({}, editingAxis, {axis: {color: new Color(color.hex)}}));
  }, [editingAxis, setEditingAxis]);
  const saveAxis = useCallback(() => {
    if (!editingAxis) {
      return;
    }
    updateAxis(editingAxis.name, editingAxis.axis);
    setEditingAxis(null);
  }, [editingAxis, setEditingAxis]);
  const cancelEditAxis = useCallback(() => setEditingAxis(null), [setEditingAxis]);
  const getAxisButtonStyle = (axis: StateType['axes']['x']) => ({
    background: '#'+colorWithLightness(axis.color, 0.9).getHexString(),
    borderColor: '#'+colorWithLightness(axis.color, 0.1).getHexString(),
  });
  const uploadTexture = useCallback(async (event: InputEvent & { target: HTMLInputElement | null }) => {
    if (!editingTexture) {
      return;
    }
    const file = event.target?.files?.[0];
    if (!file) {
      return;
    }
    setEditingTexture(merge({}, editingTexture, {loading: true}));
    const url = await uploadToImgur(file);
    setEditingTexture(merge({}, editingTexture, {texture: {url}, loading: false}));
  }, [editingTexture, setEditingTexture]);

  return <div className="overlay">
    <div className="save" onClick={showUrl}><Icon size="large"><FontAwesomeIcon icon={faSave} /></Icon></div>
    <div className="axes">
      <div className="top-bottom" onClick={editAxis('y')} style={getAxisButtonStyle(state.axes.y)}>Top to Bottom</div>
      <div className="left-right" onClick={editAxis('x')} style={getAxisButtonStyle(state.axes.x)}>Right to Left</div>
      <div className="front-back" onClick={editAxis('z')} style={getAxisButtonStyle(state.axes.z)}>Front to Back</div>
    </div>
    <div className="textures">
      <div className="square empty"></div>
      <div className="square clickable top" onClick={editTexture('y', 'positive')}>Top</div>
      <div className="square empty"></div>
      <div className="square empty"></div>
      <div className="square clickable front" onClick={editTexture('z', 'positive')}>Front</div>
      <div className="square empty"></div>
      <div className="square clickable left" onClick={editTexture('x', 'negative')}>Left</div>
      <div className="square clickable bottom" onClick={editTexture('y', 'negative')}>Bottom</div>
      <div className="square clickable right" onClick={editTexture('x', 'positive')}>Right</div>
      <div className="square empty"></div>
      <div className="square clickable back" onClick={editTexture('z', 'negative')}>Back</div>
      <div className="square empty"></div>
    </div>
    <Modal containerClassName="modal-container" className="axis-modal" active={!!editingAxis}>
      <Modal.Background onClick={cancelEditAxis} />
      <Modal.Content>
        <Notification>
          <Delete as="button" onClick={cancelEditAxis} />
          <h2>{axisPoleLabels[editingAxis?.name || 'x'].positive} to {axisPoleLabels[editingAxis?.name || 'x'].negative}</h2>
          <Field horizontal>
            <Field.Label size="normal">
              <Label>{axisPoleLabels[editingAxis?.name || 'x'].positive} Title:</Label>
            </Field.Label>
            <Field.Body>
              <Field>
                <Control>
                  <Input type="text" value={editingAxis?.axis.poles.positive.label} onChange={changeAxisPositiveLabel} />
                </Control>
              </Field>
            </Field.Body>
          </Field>
          <Field horizontal>
            <Field.Label size="normal">
              <Label>{axisPoleLabels[editingAxis?.name || 'x'].negative} Title:</Label>
            </Field.Label>
            <Field.Body>
              <Field>
                <Control>
                  <Input type="text" value={editingAxis?.axis.poles.negative.label} onChange={changeAxisNegativeLabel} />
                </Control>
              </Field>
            </Field.Body>
          </Field>
          <Field horizontal>
            <Field.Label size="normal">
              <Label>Color:</Label>
            </Field.Label>
            <Field.Body>
              <Field>
                <SliderPicker
                  color={ '#'+editingAxis?.axis.color.getHexString() }
                  onChangeComplete={ changeAxisColor }
                />
              </Field>
            </Field.Body>
          </Field>
          <Level>
            <Level.Item><Button onClick={saveAxis} color="dark">Save</Button></Level.Item>
            <Level.Item><Button onClick={cancelEditAxis} color="dark">Cancel</Button></Level.Item>
          </Level>
        </Notification>
      </Modal.Content>
    </Modal>
    <Modal containerClassName="modal-container" className="texture-modal" active={!!editingTexture}>
      <Modal.Background onClick={cancelEditTexture} />
      <Modal.Content>
        <Notification>
          <Delete as="button" onClick={cancelEditTexture} />
          <h2>{axisPoleLabels[editingTexture?.axis || 'x'][editingTexture?.pole || 'positive']} Texture</h2>
          <Field horizontal className="url">
            <Field.Label size="normal">
              <Label>Texture:</Label>
            </Field.Label>
            <Field.Body>
              <Field>
                <Control>
                  <Input type="text" value={editingTexture?.texture.url} onChange={changeTextureUrl} />
                  <FileComponent>
                    <FileComponent.Label>
                      <FileComponent.Input onChange={uploadTexture}/>
                      <FileComponent.CTA>
                        <FileComponent.Icon>
                          <FontAwesomeIcon icon={faUpload} />
                        </FileComponent.Icon>
                        <FileComponent.Label as="span">Upload</FileComponent.Label>
                      </FileComponent.CTA>
                    </FileComponent.Label>
                  </FileComponent>
                </Control>
              </Field>
            </Field.Body>
          </Field>
          <Field horizontal className="rotation">
            <Field.Label size="normal">
              <Label>Rotation:</Label>
            </Field.Label>
            <Field.Body>
              <Field>
                { [0, 90, 180, 270].map(degrees => <Button
                  key={degrees}
                  onClick={changeTextureRotation(degrees)}
                  color={editingTexture?.texture.rotation === degrees ? 'dark' : undefined}
                >{degrees}&deg;</Button>) }
              </Field>
            </Field.Body>
          </Field>
          <Level>
            <Level.Item><Button onClick={saveTextureUrl} color="dark" disabled={editingTexture?.loading}>Save</Button></Level.Item>
            <Level.Item><Button onClick={cancelEditTexture} color="dark" disabled={editingTexture?.loading}>Cancel</Button></Level.Item>
          </Level>
        </Notification>
      </Modal.Content>
    </Modal>
    <Modal containerClassName="modal-container" className="show-url-modal" active={!!showingUrl}>
      <Modal.Background onClick={cancelShowingUrl} />
      <Modal.Content>
        <Notification>
          <Delete as="button" onClick={cancelShowingUrl} />
          <h2>Share your new creation:</h2>
          <Input type="text" size="large" readOnly={true} value={showingUrl} onClick={selectUrl} />
          <Level>
            <Level.Item><Button onClick={cancelShowingUrl} color="dark">Close</Button></Level.Item>
          </Level>
        </Notification>
      </Modal.Content>
    </Modal>
  </div>;
});
