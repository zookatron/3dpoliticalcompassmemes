import {range, entries, zip, values} from 'lodash-es';
import {WebGLRenderer, PerspectiveCamera, OrthographicCamera, Vector3, Matrix4, TextureLoader, FontLoader, Font, MeshBasicMaterial, BoxGeometry,
  Mesh, Scene, Color, LineBasicMaterial, BufferGeometry, BufferAttribute, Line, TextGeometry, Shape, ExtrudeBufferGeometry, RepeatWrapping} from 'three';
import helvetiker from './fonts/helvetiker.font';
import {StateType} from './state';
import {degToRad} from './utils';

export class SceneRenderer {
  private renderer: WebGLRenderer;
  private camera: PerspectiveCamera;
  private camera2D: OrthographicCamera;
  private textureLoader: TextureLoader;
  private fontLoader: FontLoader;
  private font?: Font;
  private cube: Mesh;
  private scene: Scene;
  private overlay: Scene;

  private animationFrame = 0;
  private lon = 90;
  private lat = 5;
  private dist = 2;
  private isRotating = false;
  private isZooming = false;
  private lastMousePos = {x: 0, y: 0};
  private lastMouseScroll = 0;
  private lastZoomDist = 0;

  private axes: {
    [axis in 'x' | 'y' | 'z']: { poles: { [pole in 'negative' | 'positive']: {
      vector: Vector3, line: Line, text: Mesh, textBackground: Mesh,
    } } };
  };

  constructor(private element: HTMLElement, private state: StateType) {
    this.renderer = new WebGLRenderer({antialias: true});
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.autoClear = false;
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setClearColor(new Color(0.1, 0.1, 0.1));
    element.appendChild(this.renderer.domElement);
    this.camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    this.camera2D = new OrthographicCamera(0, window.innerWidth, 0, window.innerHeight, 0.1, 1000);
    this.camera2D.position.z = 1;

    this.textureLoader = new TextureLoader();
    this.fontLoader = new FontLoader();
    const cubeMaterials = range(6).map(() => new MeshBasicMaterial());
    const cubeGeometry = new BoxGeometry(1, 1, 1);
    this.cube = new Mesh(cubeGeometry, cubeMaterials);
    this.cube.frustumCulled = false;
    this.scene = new Scene();
    this.overlay = new Scene();
    this.scene.add(this.cube);
    this.updateTextures();

    this.fontLoader.load(helvetiker, font => {
      this.font = font;
      this.updateText();
    });

    const emptyMeshGeometry = new BoxGeometry(0, 0, 0);
    emptyMeshGeometry.computeBoundingBox();
    const emptyMesh = new Mesh(emptyMeshGeometry, new MeshBasicMaterial());
    this.axes = {} as SceneRenderer['axes'];
    for (const axis of ['x', 'y', 'z'] as ('x' | 'y' | 'z')[]) {
      this.axes[axis] = {poles: {}} as SceneRenderer['axes']['x'];
      const axisState = this.state.axes[axis];
      const material = new LineBasicMaterial({color: axisState.color});
      for (const pole of ['negative', 'positive'] as ('negative' | 'positive')[]) {
        const geometry = new BufferGeometry().setFromPoints([new Vector3(0, 0, 0), new Vector3(0, 0, 0)]);
        const line = new Line(geometry, material);
        line.frustumCulled = false;
        this.scene.add(line);
        const vector = new Vector3();
        vector[axis] = pole === 'negative' ? -1 : 1;
        this.axes[axis].poles[pole] = {vector, line, text: emptyMesh, textBackground: emptyMesh};
      }
    }

    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    this.onTouchStart = this.onTouchStart.bind(this);
    this.onTouchMove = this.onTouchMove.bind(this);
    this.onTouchEnd = this.onTouchEnd.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.onResize = this.onResize.bind(this);
    this.animate = this.animate.bind(this);

    this.element.addEventListener('mousedown', this.onMouseDown, false);
    this.element.addEventListener('mousemove', this.onMouseMove, false);
    this.element.addEventListener('mouseup', this.onMouseUp, false);
    this.element.addEventListener('touchstart', this.onTouchStart, false);
    this.element.addEventListener('touchmove', this.onTouchMove, false);
    this.element.addEventListener('touchend', this.onTouchEnd, false);
    this.element.addEventListener('wheel', this.onScroll, false);
    window.addEventListener('resize', this.onResize, false);

    this.animate();
  }

  public updateState(state: StateType) {
    this.state = state;
    this.updateTextures();
    this.updateText();
  }

  private updateTextures() {
    const axisPoleIndices = {
      x: {negative: 1, positive: 0},
      y: {negative: 3, positive: 2},
      z: {negative: 5, positive: 4},
    };
    const materials = this.cube.material as MeshBasicMaterial[];
    for (const [axisName, axis] of entries(this.state.axes)) {
      for (const [poleName, pole] of entries(axis.poles)) {
        const index = axisPoleIndices[axisName as keyof typeof axisPoleIndices][poleName as keyof typeof axisPoleIndices['x']];
        materials[index].map?.dispose();
        const texture = this.textureLoader.load(pole.texture.url);
        texture.anisotropy = this.renderer.capabilities.getMaxAnisotropy();
        texture.rotation = degToRad(pole.texture.rotation);
        texture.wrapS = texture.wrapT = RepeatWrapping;
        materials[index].map = texture;
      }
    }
  }

  private updateText() {
    if (!this.font) {
      return;
    }
    for (const [axis, axisState] of zip(values(this.axes), values(this.state.axes))) {
      if (!axis || !axisState) {
        continue;
      }
      const foreground = new Color().setHSL(0, 0, this.isColorDark(axisState.color) ? 1 : 0);
      for (const [pole, poleState] of zip(values(axis.poles), values(axisState.poles))) {
        if (!pole || !poleState) {
          continue;
        }
        this.disposeMesh(pole.text);
        const size = Math.min(
            8 * window.devicePixelRatio,
            Math.min(window.innerWidth / window.devicePixelRatio / 10, window.innerHeight / window.devicePixelRatio / 10),
        );
        pole.text = this.createText(poleState.label || '(Unnamed)', size, foreground);
        this.overlay.add(pole.text);
        const {width, height} = this.getTextSize(pole.text);
        this.disposeMesh(pole.textBackground);
        pole.textBackground = this.createRoundedRectangle(0, 0, width, height, 3, axisState.color);
        this.overlay.add(pole.textBackground);
        (pole.line.material as LineBasicMaterial).color.copy(axisState.color);
      }
    }
  }

  private animate() {
    this.animationFrame = requestAnimationFrame(this.animate);
    this.updateCamera();
    this.updatePoles();

    this.renderer.clear();
    this.renderer.render(this.scene, this.camera);
    this.renderer.clearDepth();
    this.renderer.render(this.overlay, this.camera2D);
    Transients.reset();
  }

  private updateCamera() {
    this.dist = Math.max(1.2, Math.min(6, this.dist));
    this.lat = Math.max(-85, Math.min(85, this.lat));
    const phi = degToRad(90 - this.lat);
    const theta = degToRad(this.lon);
    this.camera.position.x = this.dist * Math.sin(phi) * Math.cos(theta);
    this.camera.position.y = this.dist * Math.cos(phi);
    this.camera.position.z = this.dist * Math.sin(phi) * Math.sin(theta);
    this.camera.lookAt(0, 0, 0);
    this.camera.updateMatrixWorld();
  }

  private updatePoles() {
    const textPadding = 6;
    const poles = values(this.axes).flatMap(axis => values(axis.poles));
    for (const {vector, line, text} of poles) {
      const geometry = line.geometry as BufferGeometry;
      const attribute = geometry.attributes.position as BufferAttribute;
      const positions = attribute.array as number[];
      const center = Transients.get(Vector3).set(window.innerWidth / 2, window.innerHeight / 2, 0);
      const start = this.toScreen(Transients.get(Vector3).copy(vector).multiplyScalar(0.5));
      const end = Transients.get(Vector3).copy(start);
      end.sub(center).multiplyScalar(Math.min(Math.abs((window.innerWidth / 2) / end.x), Math.abs((window.innerHeight / 2) / end.y))).add(center);
      end.z = start.z;
      const textWidth = text.geometry.boundingBox!.max.x;
      const textHeight = text.geometry.boundingBox!.max.y;
      text.position.x = Math.max(textPadding, Math.min(window.innerWidth - textWidth - textPadding, end.x - textWidth / 2));
      text.position.y = Math.max(textPadding, Math.min(window.innerHeight - textHeight - textPadding, end.y - textHeight / 2));
      this.fromScreen(start);
      this.fromScreen(end);
      positions[0] = start.x;
      positions[1] = start.y;
      positions[2] = start.z;
      positions[3] = end.x;
      positions[4] = end.y;
      positions[5] = end.z;
      attribute.needsUpdate = true;
    }
    for (const poleA of poles) {
      for (const poleB of poles) {
        if (poleA === poleB) {
          continue;
        }
        const textA = poleA.text;
        const textB = poleB.text;
        const {width: textAWidth, height: textAHeight} = this.getTextSize(textA);
        const {width: textBWidth, height: textBHeight} = this.getTextSize(textB);
        const neededChange = this.boxIntersection(
            textA.position.x - textPadding, textA.position.y - textPadding,
            textA.position.x + textAWidth + textPadding, textA.position.y + textAHeight + textPadding,
            textB.position.x - textPadding, textB.position.y - textPadding,
            textB.position.x + textBWidth + textPadding, textB.position.y + textBHeight + textPadding,
        );
        textA.position.add(neededChange);
        textB.position.sub(neededChange);
        poleA.textBackground.position.copy(textA.position);
        poleB.textBackground.position.copy(textB.position);
      }
    }
  }

  private onMouseDown(event: MouseEvent) {
    this.isRotating = true;
    this.lastMousePos.x = event.clientX;
    this.lastMousePos.y = event.clientY;
  }

  private onMouseMove(event: MouseEvent) {
    if (!this.isRotating) {
      return;
    }
    this.lon -= (this.lastMousePos.x - event.clientX) * 0.1;
    this.lat -= (this.lastMousePos.y - event.clientY) * 0.1;
    this.lastMousePos.x = event.clientX;
    this.lastMousePos.y = event.clientY;
  }

  private onMouseUp() {
    this.isRotating = false;
  }

  private onTouchStart(event: TouchEvent) {
    event.preventDefault();
    if (event.touches.length === 1) {
      this.isRotating = true;
      this.isZooming = false;
      this.lastMousePos.x = event.touches[0].clientX;
      this.lastMousePos.y = event.touches[0].clientY;
    } else if (event.touches.length === 2) {
      this.isRotating = false;
      this.isZooming = true;
      this.lastZoomDist = Math.hypot(event.touches[0].pageX - event.touches[1].pageX, event.touches[0].pageY - event.touches[1].pageY);
    } else {
      this.isRotating = false;
      this.isZooming = false;
    }
  }

  private onTouchMove(event: TouchEvent) {
    if (this.isRotating) {
      this.lon -= (this.lastMousePos.x - event.touches[0].clientX) * 0.3;
      this.lat -= (this.lastMousePos.y - event.touches[0].clientY) * 0.3;
      this.lastMousePos.x = event.touches[0].clientX;
      this.lastMousePos.y = event.touches[0].clientY;
    } else if (this.isZooming) {
      const newZoomDist = Math.hypot(event.touches[0].pageX - event.touches[1].pageX, event.touches[0].pageY - event.touches[1].pageY);
      this.dist -= (newZoomDist - this.lastZoomDist) * 0.03;
      this.lastZoomDist = newZoomDist;
    }
  }

  private onTouchEnd() {
    this.isRotating = false;
    this.isZooming = false;
  }

  private onScroll(event: WheelEvent) {
    this.dist += Math.sign(event.deltaY) * 0.2;
  }

  private onResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.camera2D.left = 0;
    this.camera2D.right = window.innerWidth;
    this.camera2D.top = 0;
    this.camera2D.bottom = window.innerHeight;
    this.camera2D.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  private toScreen(vector: Vector3) {
    vector.project(this.camera);
    vector.x = (vector.x / 2 + 0.5) * window.innerWidth;
    vector.y = window.innerHeight - (vector.y / 2 + 0.5) * window.innerHeight;
    return vector;
  }

  private fromScreen(vector: Vector3) {
    vector.x = ((vector.x / window.innerWidth) - 0.5) * 2;
    vector.y = (((window.innerHeight - vector.y) / window.innerHeight) - 0.5) * 2;
    vector.unproject(this.camera);
    return vector;
  }

  private boxIntersection(aMinX: number, aMinY: number, aMaxX: number, aMaxY: number, bMinX: number, bMinY: number, bMaxX: number, bMaxY: number): Vector3 {
    const result = Transients.get(Vector3);
    if (aMinX >= bMaxX) {
      return result;
    }
    if (bMinX >= aMaxX) {
      return result;
    }
    if (aMinY >= bMaxY) {
      return result;
    }
    if (bMinY >= aMaxY) {
      return result;
    }
    let cantMoveX = aMinX <= 0 || bMinX <= 0 || aMaxX >= window.innerWidth || bMaxX >= window.innerWidth;
    let cantMoveY = aMinY <= 0 || bMinY <= 0 || aMaxY >= window.innerHeight || bMaxY >= window.innerHeight;
    if (cantMoveX && cantMoveY) {
      cantMoveX = false; cantMoveY = false;
    }
    const negX = cantMoveX ? Infinity : aMaxX - bMinX;
    const posX = cantMoveX ? Infinity : bMaxX - aMinX;
    const negY = cantMoveY ? Infinity : aMaxY - bMinY;
    const posY = cantMoveY ? Infinity : bMaxY - aMinY;
    if (negX <= posX && negX <= negY && negX <= posY) {
      result.set(-negX, 0, 0);
    } else if (posX <= negX && posX <= negY && posX <= posY) {
      result.set(posX, 0, 0);
    } else if (negY <= negX && negY <= posX && negY <= posY) {
      result.set(0, -negY, 0);
    } else if (posY <= negX && posY <= posX && posY <= negY) {
      result.set(0, posY, 0);
    }
    result.multiplyScalar(0.5);
    return result;
  }

  private createText(text: string, size: number, color: Color) {
    if (!this.font) {
      throw new Error('Unable to create text: Font is not loaded!');
    }
    const geometry = new TextGeometry(text, {font: this.font, size, height: 0.1});
    const material = new MeshBasicMaterial({color});
    geometry.applyMatrix4(Transients.get(Matrix4).makeRotationX(Math.PI));
    geometry.computeBoundingBox();
    geometry.translate(-geometry.boundingBox!.min.x, -geometry.boundingBox!.min.y, 0);
    geometry.computeBoundingBox();
    return new Mesh(geometry, material);
  }

  private getTextSize(text: Mesh) {
    return {width: text.geometry.boundingBox!.max.x, height: text.geometry.boundingBox!.max.y};
  }

  private createRoundedRectangle(x: number, y: number, width: number, height: number, radius: number, color: Color) {
    const shape = new Shape();
    shape.moveTo( x, y + radius );
    shape.lineTo( x, y + height - radius );
    shape.quadraticCurveTo( x, y + height, x + radius, y + height );
    shape.lineTo( x + width - radius, y + height );
    shape.quadraticCurveTo( x + width, y + height, x + width, y + height - radius );
    shape.lineTo( x + width, y + radius );
    shape.quadraticCurveTo( x + width, y, x + width - radius, y );
    shape.lineTo( x + radius, y );
    shape.quadraticCurveTo( x, y, x, y + radius );
    const geometry = new ExtrudeBufferGeometry(shape, {depth: 0.1});
    const material = new MeshBasicMaterial({color});
    return new Mesh(geometry, material);
  }

  private isColorDark(color: Color) {
    // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
    const hsp = Math.sqrt(0.299 * (color.r * color.r) +0.587 * (color.g * color.g) +0.114 * (color.b * color.b));
    return hsp < 0.5;
  }

  private disposeMesh(mesh: Mesh | Line) {
    mesh.parent?.remove(mesh);
    mesh.geometry.dispose();
    if ('length' in mesh.material) {
      mesh.material.map(material => material.dispose());
    } else {
      mesh.material.dispose();
    }
  }

  public dispose() {
    this.disposeMesh(this.cube);
    values(this.axes).flatMap(axis => values(axis.poles).flatMap(pole => [pole.line, pole.text, pole.textBackground])).map(this.disposeMesh);
    this.scene.dispose();
    this.overlay.dispose();
    this.renderer.dispose();
    this.element.removeChild(this.renderer.domElement);

    this.element.removeEventListener('mousedown', this.onMouseDown, false);
    this.element.removeEventListener('mousemove', this.onMouseMove, false);
    this.element.removeEventListener('mouseup', this.onMouseUp, false);
    this.element.removeEventListener('touchstart', this.onTouchStart, false);
    this.element.removeEventListener('touchmove', this.onTouchMove, false);
    this.element.removeEventListener('touchend', this.onTouchEnd, false);
    this.element.removeEventListener('wheel', this.onScroll, false);
    window.removeEventListener('resize', this.onResize, false);
    cancelAnimationFrame(this.animationFrame);
  }
}

class Transients {
  private static klasses = new Map<new() => unknown, { instances: unknown[], count: number }>();

  public static get<T>(klass: new () => T): T {
    let data = this.klasses.get(klass);
    if (!data) {
      data = {instances: [], count: 0};
    }
    if (data.count === data.instances.length) {
      data.instances.push(new klass());
    }
    data.count += 1;
    return data.instances[data.count - 1] as T;
  }

  public static reset() {
    for (const klass of this.klasses) {
      klass[1].count = 0;
    }
  }
}
