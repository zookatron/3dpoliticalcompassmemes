declare module '*.scss' {
  const styles: { [key: string]: string };
  export default styles;
}

declare module '*.font' {
  const path: string;
  export default path;
}

declare const IMGUR_CLIENT_ID: string;
