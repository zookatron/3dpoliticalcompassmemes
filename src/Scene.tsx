import React, {useEffect, useCallback} from 'react';
import {toJS} from 'mobx';
import {withState, StateType, updateInteracted} from './state';
import {SceneRenderer} from './SceneRenderer';
import {getLeaves} from './utils';

let renderer: SceneRenderer | null = null;

export const Scene = withState(({state}: { state: StateType }) => {
  const sceneRefChange = useCallback((scene: HTMLDivElement | null) => {
    if (!scene) {
      return;
    }
    if (renderer) {
      renderer.dispose();
    }
    renderer = new SceneRenderer(scene, toJS(state));
  }, []);

  useEffect(() => {
    renderer?.updateState(toJS(state));
  }, [getLeaves(toJS(state.axes))]);

  const interact = useCallback(state.interacted ? () => {/* Do nothing */} : () => updateInteracted(true), [state.interacted]);

  return <div className="scene" ref={sceneRefChange} onMouseDown={interact} onTouchStart={interact} onWheel={interact}></div>;
});
